package com.example.mongoPrueba.service.Impl;

import com.example.mongoPrueba.dto.ClientDTO;
import com.example.mongoPrueba.dto.ProductDTO;
import com.example.mongoPrueba.model.Client;
import com.example.mongoPrueba.repository.ClientRepository;
import com.example.mongoPrueba.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public List<Client> getAllClients() {
       return clientRepository.getAllClients();
    }

    @Override
    public Client createClient(ClientDTO clientDTO) {
        String paymentMethod = clientDTO.getPaymentMethod();
        if (!Arrays.asList("Efectivo", "Visa", "Mastercard").contains(paymentMethod)) {
            throw new IllegalArgumentException("No se acepta el metodo de pago " + paymentMethod);
        }

        int totalQuantity = 0;
        for (ProductDTO productDTO : clientDTO.getProductsDTOS()) {
            totalQuantity += productDTO.getQuantity();
        }
        if (totalQuantity < 10) {
            throw new IllegalArgumentException("No se pueden comprar menos de 10 productos");
        }

        Client client = new Client(clientDTO);
        double total = 0.0;
        for (ProductDTO productDTO : clientDTO.getProductsDTOS()) {
            double subTotal = productDTO.getPrice() * productDTO.getQuantity();
            total += subTotal;
        }
        double discount = 0.0;
        if (paymentMethod.equals("Efectivo")) {
            discount = total * 0.2;
        } else if (paymentMethod.equals("Visa")) {
            discount = total * 0.1;
        }

        client.setTotal(total - discount);
        clientRepository.saveClient(client);
        return client;
    }

    @Override
    public List<Client> getClientsByName(String name) {
        return clientRepository.getClientsByName(name);
    }

    @Override
    public List<Client> getClientsByStreetName(String streetName) {
        return clientRepository.getClientsByNameStreet(streetName);
    }

    @Override
    public List<Client> getClientsByTotal(double total) {
        return clientRepository.getClientByTotal(total);
    }
}
