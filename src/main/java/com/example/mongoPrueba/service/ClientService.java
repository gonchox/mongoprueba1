package com.example.mongoPrueba.service;

import com.example.mongoPrueba.dto.ClientDTO;
import com.example.mongoPrueba.model.Client;

import java.util.List;

public interface ClientService {
    List<Client> getAllClients();
    Client createClient(ClientDTO clientDTO);
    List<Client> getClientsByName(String name);
    List<Client> getClientsByStreetName(String streetName);

    List<Client> getClientsByTotal(double total);
}
