package com.example.mongoPrueba.controller;

import com.example.mongoPrueba.dto.ClientDTO;
import com.example.mongoPrueba.model.Client;
import com.example.mongoPrueba.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/v1/clients")
public class ClientController {
    @Autowired
    private ClientService clientService;

    @GetMapping("/names/{name}")
    public ResponseEntity<List<Client>> getClientsByName(@PathVariable String name){
        try{
            List<Client> clients = clientService.getClientsByName(name);
            return ResponseEntity.status(HttpStatus.OK).body(clients);

        }catch (Exception ex){
            throw ex;
        }
    }

    @GetMapping("/nameStreets/{nameStreet}")
    public ResponseEntity<List<Client>> getClientsByNameStreet(@PathVariable String nameStreet){
        try{
            List<Client> clients = clientService.getClientsByStreetName(nameStreet);
            return ResponseEntity.status(HttpStatus.OK).body(clients);

        }catch (Exception ex){
            throw ex;
        }
    }

    @PostMapping("")
    public ResponseEntity<Client> createClient(@RequestBody ClientDTO clientDTO){
        try{
            Client client = clientService.createClient(clientDTO);
            return ResponseEntity.status(HttpStatus.CREATED).body(client);

        }catch(Exception ex){
            throw ex;
        }
    }

    @GetMapping("")
    public ResponseEntity<List<Client>> getClients(){
        try{
            List<Client> clients = clientService.getAllClients();
            return ResponseEntity.status(HttpStatus.OK).body(clients);

        }catch (Exception ex){
            throw ex;
        }
    }
    @GetMapping("/totals/{total}")
    public ResponseEntity<List<Client>> getClientsByTotal(@PathVariable double total){
        try{
            List<Client> clients = clientService.getClientsByTotal(total);
            return ResponseEntity.status(HttpStatus.OK).body(clients);

        }catch (Exception ex){
            throw ex;
        }
    }
}
