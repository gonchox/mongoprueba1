package com.example.mongoPrueba.dto;

import lombok.Data;

@Data
public class UbigeoDTO {
    private String department;
    private String province;
    private String district;
}
