package com.example.mongoPrueba.dto;

import lombok.Data;

import java.util.List;

@Data
public class ClientDTO {
    private String name;
    private String lastName;
    private String paymentMethod;
    private List<AddressDTO> addressesDTOS;
    private List<ProductDTO> productsDTOS;
}
