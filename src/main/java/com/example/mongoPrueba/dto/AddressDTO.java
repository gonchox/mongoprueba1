package com.example.mongoPrueba.dto;

import lombok.Data;

@Data
public class AddressDTO {
    private String nameStreet;
    private String number;
    private String reference;
    private UbigeoDTO ubigeoDTO;
}
