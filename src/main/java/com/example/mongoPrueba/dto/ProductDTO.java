package com.example.mongoPrueba.dto;

import lombok.Data;

@Data
public class ProductDTO {
    private String name;
    private String description;
    private double price;
    private String category;
    private int quantity;
}
