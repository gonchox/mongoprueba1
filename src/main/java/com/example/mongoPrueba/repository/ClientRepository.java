package com.example.mongoPrueba.repository;

import com.example.mongoPrueba.model.Client;

import java.util.List;

public interface ClientRepository {

    Client saveClient(Client client);

    List<Client> getAllClients();

    List<Client> getClientsByName(String name);

    List<Client> getClientsByNameStreet(String nameStreet);

    List<Client> getClientByTotal(double total);


}
