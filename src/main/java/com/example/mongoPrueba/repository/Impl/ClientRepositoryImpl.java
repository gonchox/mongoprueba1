package com.example.mongoPrueba.repository.Impl;

import com.example.mongoPrueba.model.Client;
import com.example.mongoPrueba.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;


import java.util.List;

@Repository
public class ClientRepositoryImpl implements ClientRepository {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public Client saveClient(Client client) {
        mongoTemplate.insert(client,"Client");
        return client;
    }

    @Override
    public List<Client> getAllClients() {
        return mongoTemplate.findAll(Client.class,"Client");
    }

    @Override
    public List<Client> getClientsByName(String name) {
        Query query = new Query();
        query.addCriteria(Criteria.where("name").is(name));
        return mongoTemplate.find(query, Client.class,"Client");
    }

    @Override
    public List<Client> getClientsByNameStreet(String nameStreet) {
        Query query = new Query();
        query.addCriteria(Criteria.where("addressDTOS.nameStreet").is(nameStreet));
        return mongoTemplate.find(query, Client.class,"Client");
    }

    @Override
    public List<Client> getClientByTotal(double total) {
        Query query = new Query(Criteria.where("total").gt(total));
        List<Client> clients = mongoTemplate.find(query, Client.class, "Client");
        return clients;
    }
}
