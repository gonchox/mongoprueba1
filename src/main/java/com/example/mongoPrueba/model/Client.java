package com.example.mongoPrueba.model;

import com.example.mongoPrueba.dto.AddressDTO;
import com.example.mongoPrueba.dto.ClientDTO;
import com.example.mongoPrueba.dto.ProductDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document
@NoArgsConstructor
@AllArgsConstructor
public class Client {
    private String id;
    private String name;
    private String lastName;
    private String paymentMethod;
    private double total;
    private List<ProductDTO> productDTOS;
    private List<AddressDTO> addressDTOS;

    public Client(ClientDTO clientDTO){
        this.name = clientDTO.getName();
        this.lastName = clientDTO.getLastName();
        this.paymentMethod = clientDTO.getPaymentMethod();
        this.productDTOS = clientDTO.getProductsDTOS();
        this.addressDTOS = clientDTO.getAddressesDTOS();
    }
}
