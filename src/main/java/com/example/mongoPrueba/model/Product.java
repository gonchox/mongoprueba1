package com.example.mongoPrueba.model;

import lombok.Data;

@Data
public class Product {
    private String name;
    private String description;
    private double price;
    private String category;
    private int quantity;
}
