package com.example.mongoPrueba.model;

import lombok.Data;

@Data
public class Address {
    private String nameStreet;
    private String number;
    private String reference;
    private Ubigeo ubigeo;
}
